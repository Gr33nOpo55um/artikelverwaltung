package com.company;

class Main {

    public static void main(String[] args) {
        Warehouse myStock = new Warehouse();

        Software windows_7 = new Software("Windows 7", 100, 200, 123, "002");
        Warehouse.addWarehouseStock(windows_7);

        Software windows_8 = new Software("Windows 8.1", 100, 200, 123, "003");
        Warehouse.addWarehouseStock(windows_8);

        Hardware macbook = new Hardware("Macbook", 1000, 2000, 2045, 50, 1, "001");
        Warehouse.addWarehouseStock(macbook);

        ItemCounter.getItemCount();
        windows_7.print();
        windows_8.print();
        System.out.println(Item.getInstanceCount());
        Warehouse.countWarehouseValue();
        Warehouse.countWarehouseCost();
        Warehouse.printAttribute();
    }
}
