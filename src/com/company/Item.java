package com.company;

/**
 * Created by admin on 17.02.15.
 */
abstract class Item {

    private static int instanceCount = 0;
    public String articleNumberCategoriesTop;
    public String articleNumberObject;
    public String articleNumberArticle;
    private String name;
    private int sellingPrice;
    private int vendorPrice;
    private int quantityItem;

    Item(String name, int sellingPrice, int vendorPrice, int quantityItem) {
        ItemCounter.increaseItemCount();
        this.name = name;
        this.sellingPrice = sellingPrice;
        this.vendorPrice = vendorPrice;
        this.quantityItem = quantityItem;
        this.articleNumberCategoriesTop = "001";
        instanceCount++;


    }

    public static int getInstanceCount() {
        return instanceCount;
    }


    public String getArticleNumberCategoriesTop() {
        return articleNumberCategoriesTop;
    }

    public void setArticleNumberCategoriesTop(String articleNumberCategoriesTop) {
        this.articleNumberCategoriesTop = articleNumberCategoriesTop;
    }

    public String getArticleNumberObject() {
        return articleNumberObject;
    }

    public void setArticleNumberObject(String articleNumberObject) {
        this.articleNumberObject = articleNumberObject;
    }

    public String getArticleNumberArticle() {
        return articleNumberArticle;
    }

    public void setArticleNumberArticle(String articleNumberArticle) {
        this.articleNumberArticle = articleNumberArticle;
    }

    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSellingPrice() {
        return sellingPrice;
    }

    public void setSellingPrice(int sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public int getVendorPrice() {
        return vendorPrice;
    }

    public void setVendorPrice(int vendorPrice) {
        this.vendorPrice = vendorPrice;
    }

    public void print() {
        System.out.println("Article Number: " + this.getArticleNumberCategoriesTop() + "." + this.getArticleNumberObject() + "." + this.getArticleNumberArticle());
        System.out.println("Item name: " + this.getName());
        System.out.println("Selling price: " + this.getSellingPrice());
        System.out.println("Vendor price: " + this.getVendorPrice());

    }
}
