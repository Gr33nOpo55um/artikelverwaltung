package com.company;

/**
 * Created by stegmuellerp on 18.02.2015.
 */

import java.util.UUID;

public class SoftwareKeyGenerator {
    static UUID serialNumber;

    public static UUID getSerialNumber() {
        serialNumber = UUID.randomUUID();

        return serialNumber;
    }

}
