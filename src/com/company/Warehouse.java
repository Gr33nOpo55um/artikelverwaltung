package com.company;

import java.util.LinkedList;

/**
 * Created by admin on 17.02.15.
 */
class Warehouse {

    private static LinkedList<Item> warehouseStock;

    public Warehouse() {
        warehouseStock = new LinkedList<Item>();
    }

    public static LinkedList<Item> getWarehouseStock() {
        return warehouseStock;
    }


    public static void addWarehouseStock(Item item) {

        warehouseStock.add(item);
    }

    public static void countWarehouseValue() {
        int warehouseValue = 0;

        for (Item item : warehouseStock) {
            warehouseValue += item.getSellingPrice();
        }

        System.out.println("Stock value: " + warehouseValue);
    }

    public static void printAttribute() {

        for (Item item : warehouseStock) {
            item.print();
        }
    }

    public static void countWarehouseCost() {
        int warehouseCost = 0;

        for (Item item : warehouseStock) {
            warehouseCost += item.getVendorPrice();
        }

        System.out.println("Stock value: " + warehouseCost);
    }


}
