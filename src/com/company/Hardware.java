package com.company;

/**
 * Created by admin on 17.02.15.
 */
public class Hardware extends Item {

    private int ram;
    private int disk;

    public Hardware(String name, int sellingPrice, int vendorPrice, int ram, int disk, int quantifyItem, String articleNumberArticle) {
        super(name, sellingPrice, vendorPrice, quantifyItem);
        this.ram = ram;
        this.disk = disk;
        this.articleNumberObject = "0002";
        this.articleNumberArticle = articleNumberArticle;

    }

    int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    int getDisk() {
        return disk;
    }

    public void setDisk(int disk) {
        this.disk = disk;
    }

    public void print() {
        super.print();
        System.out.println("Ram: " + getRam());
        System.out.println("Disk: " + getDisk());

    }
}
