package com.company;

import java.util.UUID;

/**
 * Created by admin on 17.02.15.
 */
public class Software extends Item {
    private UUID licenseKey;

    public Software(String name, int sellingPrice, int vendorPrice, int quantityItem, String articleNumberArticle) {
        super(name, sellingPrice, vendorPrice, quantityItem);
        this.licenseKey = SoftwareKeyGenerator.getSerialNumber();
        this.articleNumberObject = "0001";
        this.articleNumberArticle = articleNumberArticle;
    }

    UUID getLicenseKey() {
        return licenseKey;
    }

    public void print() {
        super.print();
        System.out.println("License key: " + getLicenseKey());

    }

}
