package com.company;

/**
 * Created by stegmuellerp on 18.02.2015.
 */
class ItemCounter {

    private static int ItemCount = 1;


    public static int getItemCount() {
        return ItemCount;
    }


    public static void increaseItemCount() {
        ItemCount++;
    }
}
